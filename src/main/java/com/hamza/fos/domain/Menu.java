package com.hamza.fos.domain;

import com.hamza.fos.domain.products.Product;

import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by oksana on 07.06.16.
 */
public final class Menu {

    private final List<Product> dishes;

    public Menu(List<Product> dishes) {
        this.dishes = dishes;
    }

    public Product getDish(int dishIndex) {
        return dishes.get(dishIndex);
    }

    public List<Product> getDishes(){
        return dishes;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        IntStream.range(0, dishes.size())
                .forEach(i -> stringBuilder
                        .append(i)
                        .append(".")
                        .append(dishes.get(i))
                        .append("\r\n"));

        return stringBuilder.toString();
    }
}
