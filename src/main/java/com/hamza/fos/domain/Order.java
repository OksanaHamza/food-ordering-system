package com.hamza.fos.domain;

import com.hamza.fos.domain.products.Dish;
import com.hamza.fos.domain.products.Product;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by oksana on 09.06.16.
 */
public class Order {

    private Lunch lunch;
    private Product drink;
    private Set<Product> drinkAdditives;
    private BigDecimal total = new BigDecimal(0);

    public BigDecimal getPrice() {
        if (lunch != null) {
            total = total.add(lunch.getTotalPrice());
        }

        if (drink != null) {
            total = total.add(drink.getPrice());
            if (drinkAdditives != null && !drinkAdditives.isEmpty()) {
                drinkAdditives.stream().forEach(additive -> total.add(additive.getPrice()));
            }
        }
        return total;
    }

    public void addLunch(Lunch lunch) {
        this.lunch = lunch;
    }

    public void addDrink(Product drink) {
        this.drink = drink;
    }

    public boolean isLunchPresent() {
        return lunch != null;
    }

    public boolean isDrinkPresent() {
        return drink != null;
    }

    public void addDrinkAdditive(Product additive) {
        if (drinkAdditives == null) {
            drinkAdditives = new HashSet<>();
        }
        drinkAdditives.add(additive);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (lunch != null) {
            stringBuilder.append(lunch).append(" ");
        }

        if (drink != null) {
            stringBuilder.append("\r\n").append("Drink: ").append(drink);
            if (drinkAdditives != null) {
                drinkAdditives.stream().forEach(additive -> stringBuilder.append(" with ").append(additive));
            }

        }

        stringBuilder.append("\r\n").append("Total: ").append(getPrice().doubleValue());
        return stringBuilder.toString();
    }

    //We can't order any dish separately
    public class Lunch {

        private Product mainCourse;
        private Product dessert;

        public void addMeal(Product product) {
            if (product != null) {
                Dish dish = (Dish) product;

                switch (dish.getDishType()) {
                    case MAIN_COURSE:
                        if (mainCourse == null) {
                            mainCourse = dish;
                        } else {
                            System.out.println("Your lunch already contains a main course");
                        }
                        break;
                    case DESSERT:
                        if (dessert == null) {
                            dessert = dish;
                        } else {
                            System.out.println("Your lunch already contains a dessert");
                        }
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown dish type: " + dish.getDishType());
                }
            }

        }

        public BigDecimal getTotalPrice() {
            return mainCourse.getPrice().add(dessert.getPrice());
        }

        @Override
        public String toString() {
            return "Lunch: mainCourse = " + mainCourse +
                    ", dessert = " + dessert;
        }
    }
}
