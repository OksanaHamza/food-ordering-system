package com.hamza.fos.domain.cousines;

import com.hamza.fos.domain.Menu;

/**
 * Created by oksana on 09.06.16.
 */
public class ItalianCuisine extends Cuisine {
    private static final String CUISINE_NAME = "Italian cuisine";

    public ItalianCuisine(Menu menu) {
        super(menu);
    }

    @Override
    public String getName() {
        return CUISINE_NAME;
    }
}
