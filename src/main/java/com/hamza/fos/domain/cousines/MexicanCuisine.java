package com.hamza.fos.domain.cousines;

import com.hamza.fos.domain.Menu;

/**
 * Created by oksana on 09.06.16.
 */
public class MexicanCuisine extends Cuisine {
    private static final String CUISINE_NAME = "Mexican cuisine";

    public MexicanCuisine(Menu menu) {
        super(menu);
    }

    @Override
    public String getName() {
        return CUISINE_NAME;
    }
}
