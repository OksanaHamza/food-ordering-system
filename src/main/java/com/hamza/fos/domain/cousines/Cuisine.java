package com.hamza.fos.domain.cousines;

import com.hamza.fos.domain.Menu;

/**
 * Created by oksana on 07.06.16.
 */
public abstract class Cuisine {

    private final Menu menu;

    public Cuisine(Menu menu) {
        this.menu = menu;
    }

    public Menu getMenu() {
        return menu;
    }

    public abstract String getName();

    @Override
    public String toString() {
        return getName() + " MENU " + "\r\n" + menu;

    }
}
