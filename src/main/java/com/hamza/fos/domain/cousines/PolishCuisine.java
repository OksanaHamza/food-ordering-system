package com.hamza.fos.domain.cousines;

import com.hamza.fos.domain.Menu;

/**
 * Created by oksana on 08.06.16.
 */
public class PolishCuisine extends Cuisine {
    private static final String CUISINE_NAME = "Polish cuisine";

    public PolishCuisine(Menu menu) {
        super(menu);
    }

    @Override
    public String getName() {
        return CUISINE_NAME;
    }
}
