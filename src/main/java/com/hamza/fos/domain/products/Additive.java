package com.hamza.fos.domain.products;

import java.math.BigDecimal;

/**
 * Created by oksana on 12.06.16.
 */
public class Additive extends Product {

    public Additive(String name, BigDecimal price) {
        super(name, price);
    }
}
