package com.hamza.fos.domain.products;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import java.math.BigDecimal;

/**
 * Created by oksana on 08.06.16.
 */
public abstract class Product {
    private final String name;
    private final BigDecimal price;

    public Product(String name, BigDecimal price) {
        if (Strings.isNullOrEmpty(name)) throw new IllegalArgumentException("Name should not be null or empty.");
        if (price == null) throw new IllegalArgumentException("Price should not be null");
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equal(name, product.name) &&
                Objects.equal(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, price);
    }

    @Override
    public String toString() {
        return name + " - price " + price.doubleValue();
    }
}
