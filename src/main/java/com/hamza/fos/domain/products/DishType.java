package com.hamza.fos.domain.products;

/**
 * Created by oksana on 07.06.16.
 */
public enum DishType {
    MAIN_COURSE("Main course"),
    DESSERT("Dessert");

    private String value;

    DishType(String value) {

        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
