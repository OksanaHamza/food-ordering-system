package com.hamza.fos.domain.products;


import java.math.BigDecimal;

/**
 * Created by oksana on 08.06.16.
 */
public class Drink extends Product {

    public Drink(String name, BigDecimal price) {
        super(name, price);
    }

}
