package com.hamza.fos.domain.products;

import com.google.common.base.Objects;

import java.math.BigDecimal;

/**
 * Created by oksana on 07.06.16.
 */
public class Dish extends Product {
    private final DishType dishType;

    public Dish(String name, BigDecimal price, DishType dishType) {
        super(name, price);
        this.dishType = dishType;
    }

    public DishType getDishType() {
        return dishType;
    }

    @Override
    public String toString() {
        return "*" + dishType.getValue() + "* " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Dish dish = (Dish) o;
        return dishType == dish.dishType;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), dishType);
    }

}
