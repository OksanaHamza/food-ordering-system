package com.hamza.fos;

import com.hamza.fos.domain.Order;
import com.hamza.fos.domain.cousines.Cuisine;
import com.hamza.fos.domain.products.Dish;
import com.hamza.fos.domain.products.DishType;
import com.hamza.fos.domain.products.Product;
import com.hamza.fos.services.AdditivesProvider;
import com.hamza.fos.services.CuisinesProvider;
import com.hamza.fos.services.DrinksProvider;

import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;


/**
 * Created by oksana on 07.06.16.
 */
public class FoodOrderingApp {
    private Scanner scanner = new Scanner(System.in);

    private Restaurant restaurant;
    private Order order;
    private boolean isWaitingForInput = true;


    public FoodOrderingApp(CuisinesProvider cuisinesProvider, DrinksProvider drinksProvider, AdditivesProvider additivesProvider) {
        restaurant = new Restaurant.Builder()
                .withCuisines(cuisinesProvider.getCuisines())
                .withDrinks(drinksProvider.getDrinks())
                .withAdditives(additivesProvider.getAdditives())
                .build();
    }

    public static void main(String[] args) {
        FoodOrderingApp foodOrderingApp = new FoodOrderingApp(
                CuisinesProvider.getInstance(),
                DrinksProvider.getInstance(),
                AdditivesProvider.getInstance());

        foodOrderingApp.mainMenu();

    }

    private void mainMenu() {
        System.out.println("Welcome to the Food Ordering System");
        System.out.println("To choose an option, press corresponding number.\r\n");
        while (isWaitingForInput) {
            System.out.println("What would you like to order?\r\n");
            System.out.println("0.Lunch");
            System.out.println("1.Drink");
            order = new Order();
            switch (scanOption()) {
                case 0:
                    cuisinesMenu();
                    break;
                case 1:
                    chooseDrink();
                    break;
                default:
                    wrongInputMessage();
                    break;
            }
        }

    }

    private void cuisinesMenu() {
        isWaitingForInput = true;
        System.out.println("To choose a lunch, please select a cuisine.\r\n");
        List<Cuisine> cuisines = restaurant.getCuisines();
        IntStream.range(0, cuisines.size()).forEach(i -> System.out.println(i + "." + cuisines.get(i).getName()));
        System.out.println("\r\nTo finish, press " + cuisines.size() + "\r\n");

        while (isWaitingForInput) {
            final int index = scanOption();
            if (index == cuisines.size()) {
                finishOrder();
                isWaitingForInput = false;
            } else if (index < 0 || index >= cuisines.size()) {
                wrongInputMessage();
            } else {
                Cuisine selected = cuisines.get(index);
                cuisineMenu(selected);
                chooseLunchMainCourse(selected);
                if (order.isDrinkPresent()) {
                    finishOrder();
                } else {
                    chooseDrink();
                }
            }
        }
    }

    private void chooseLunchMainCourse(Cuisine selected) {
        isWaitingForInput = true;
        System.out.println("Lunch may consist of a main course and a dessert");
        System.out.println("What main course would you like?");
        while (isWaitingForInput) {
            final int input = scanOption();
            if (isValidInput(input, selected.getMenu().getDishes())) {
                Dish dish = (Dish) selected.getMenu().getDish(input);
                if (isMainCourse(dish)) {
                    Order.Lunch lunch = order.new Lunch();
                    lunch.addMeal(selected.getMenu().getDish(input));
                    order.addLunch(lunch);
                    chooseLunchDessert(selected, lunch);
                }else{
                    wrongInputMessage();
                }
            } else {
                wrongInputMessage();
            }
        }
    }

    private void chooseLunchDessert(Cuisine selected, Order.Lunch lunch) {
        isWaitingForInput = true;
        System.out.println("Main course has been added to your lunch");
        System.out.println("What dessert would you like?");
        while (isWaitingForInput) {
            final int input = scanOption();
            if (isValidInput(input, selected.getMenu().getDishes())) {
                Dish dish = (Dish) selected.getMenu().getDish(input);
                if (!isMainCourse(dish)) {
                    lunch.addMeal(selected.getMenu().getDish(input));
                    System.out.println("A dessert has been added to your lunch");
                    isWaitingForInput = false;
                }else{
                    wrongInputMessage();
                }
            } else {
                wrongInputMessage();
            }
        }
    }

    private void chooseDrink() {
        isWaitingForInput = true;
        System.out.println("What drink would you like?\r\n");
        List<Product> drinks = restaurant.getDrinks();
        IntStream.range(0, drinks.size()).forEach(i -> System.out.println(i + "." + drinks.get(i)));
        System.out.println("(To finish your order, press " + drinks.size() + ")");
        while (isWaitingForInput) {
            final int input = scanOption();
            if (input == drinks.size()) {
                finishOrder();
                isWaitingForInput = false;
            } else if (isValidInput(input, restaurant.getDrinks())) {
                order.addDrink(drinks.get(input));
                System.out.println("A drink was added to your order\r\n");
                chooseAdditive();
            } else {
                wrongInputMessage();
            }
        }

    }

    private void chooseAdditive() {
        isWaitingForInput = true;
        System.out.println("Would you like to add anything else to your drink?\r\n");
        List<Product> additives = restaurant.getAdditives();
        IntStream.range(0, additives.size()).forEach(i -> System.out.println(i + "." + additives.get(i)));
        System.out.println("(To return or finish, press " + additives.size() + ")");
        while (isWaitingForInput) {
            final int input = scanOption();
            if (input == additives.size()) {
                if (order.isLunchPresent()) {
                    finishOrder();
                } else {
                    cuisinesMenu();
                }
                isWaitingForInput = false;

            } else if (isValidInput(input, restaurant.getAdditives())) {
                order.addDrinkAdditive(additives.get(input));
                System.out.println("A drink additive was added to your order\r\n");
                chooseAdditive();
            } else {
                wrongInputMessage();
            }
        }
    }

    private void finishOrder() {
        System.out.println("Your order");
        System.out.println(order);
    }

    private void cuisineMenu(Cuisine cuisine) {
        System.out.println(cuisine);
    }

    private void wrongInputMessage() {
        System.out.println("There is no such option\r\nTry again");
    }

    private boolean isValidInput(int input, List<? extends Product> list) {
        return !(input < 0 || input >= list.size());
    }

    private boolean isMainCourse(Dish dish) {
        return dish.getDishType() == DishType.MAIN_COURSE;
    }

    private int scanOption() {
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        } else {
            scanner.next();
            return -1;
        }
    }

}

