package com.hamza.fos.services;

import com.hamza.fos.domain.cousines.Cuisine;
import com.hamza.fos.domain.cousines.ItalianCuisine;
import com.hamza.fos.domain.cousines.MexicanCuisine;
import com.hamza.fos.domain.cousines.PolishCuisine;

import java.util.Arrays;
import java.util.List;

/**
 * Created by oksana on 07.06.16.
 */
public class CuisinesProvider {
    private static final CuisinesProvider INSTANCE = new CuisinesProvider();
    private final MenuProvider menuProvider;

    private CuisinesProvider() {
        this.menuProvider = MenuProvider.getInstance();
    }

    public static CuisinesProvider getInstance() {
        return INSTANCE;
    }

    public List<Cuisine> getCuisines() {
        return Arrays.asList(
                new PolishCuisine(menuProvider.getPolishMenu()),
                new ItalianCuisine(menuProvider.getItalianMenu()),
                new MexicanCuisine(menuProvider.getMexicanMenu()));
    }
}
