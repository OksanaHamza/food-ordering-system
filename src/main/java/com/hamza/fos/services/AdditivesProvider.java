package com.hamza.fos.services;

import com.hamza.fos.domain.products.Additive;
import com.hamza.fos.domain.products.Product;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by oksana on 07.06.16.
 */
public class AdditivesProvider {
    private static final AdditivesProvider INSTANCE = new AdditivesProvider();

    private AdditivesProvider() {
    }

    public static AdditivesProvider getInstance() {
        return INSTANCE;
    }

    public List<Product> getAdditives() {
        return Arrays.asList(
                new Additive("Lemon", new BigDecimal(0.5)),
                new Additive("Ice", new BigDecimal(0)));
    }
}
