package com.hamza.fos.services;

import com.hamza.fos.domain.Menu;
import com.hamza.fos.domain.products.Dish;
import com.hamza.fos.domain.products.DishType;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Created by oksana on 09.06.16.
 */
public class MenuProvider {
    private static final MenuProvider INSTANCE = new MenuProvider();

    private MenuProvider() {
    }

    public static MenuProvider getInstance() {
        return INSTANCE;
    }

    public Menu getPolishMenu() {
        return new Menu(Arrays.asList(
                new Dish("Pierogi", new BigDecimal(2), DishType.MAIN_COURSE),
                new Dish("Bigos", new BigDecimal(4.5), DishType.MAIN_COURSE),
                new Dish("Kotlet schabowy", new BigDecimal(3.55), DishType.MAIN_COURSE),
                new Dish("Golonka", new BigDecimal(6), DishType.MAIN_COURSE),
                new Dish("Gołąbki", new BigDecimal(4.2), DishType.MAIN_COURSE),
                new Dish("Cheesecake", new BigDecimal(5.4), DishType.DESSERT)));
    }

    public Menu getItalianMenu() {
        return new Menu(Arrays.asList(new Dish("Risotto", new BigDecimal(3.3), DishType.MAIN_COURSE),
                new Dish("Pizza", new BigDecimal(5.55), DishType.MAIN_COURSE),
                new Dish("Cotoletta", new BigDecimal(2.4), DishType.MAIN_COURSE),
                new Dish("Pasta", new BigDecimal(3.4), DishType.MAIN_COURSE),
                new Dish("Ribollita", new BigDecimal(2.4), DishType.MAIN_COURSE),
                new Dish("Chocolate cake", new BigDecimal(2.4), DishType.DESSERT)));
    }

    public Menu getMexicanMenu() {
        return new Menu(Arrays.asList(new Dish("Taco", new BigDecimal(1.2), DishType.MAIN_COURSE),
                new Dish("Enchiladas", new BigDecimal(3), DishType.MAIN_COURSE),
                new Dish("Carnitas", new BigDecimal(4), DishType.MAIN_COURSE),
                new Dish("Al pastor", new BigDecimal(4.5), DishType.MAIN_COURSE),
                new Dish("Cemita", new BigDecimal(3.7), DishType.MAIN_COURSE),
                new Dish("Cake", new BigDecimal(7), DishType.DESSERT)));
    }
}
