package com.hamza.fos.services;

import com.hamza.fos.domain.products.Drink;
import com.hamza.fos.domain.products.Product;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by oksana on 07.06.16.
 */
public class DrinksProvider {
    private static final DrinksProvider INSTANCE = new DrinksProvider();

    private DrinksProvider() {
    }

    public static DrinksProvider getInstance() {
        return INSTANCE;
    }

    public List<Product> getDrinks() {
        return Arrays.asList(new Drink("Coffee", new BigDecimal(2.40)),
                new Drink("Cappuccino", new BigDecimal(3.40)),
                new Drink("Tea", new BigDecimal(1.40)));
    }
}
