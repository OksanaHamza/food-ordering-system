package com.hamza.fos;

import com.hamza.fos.domain.cousines.Cuisine;
import com.hamza.fos.domain.products.Product;

import java.util.List;

/**
 * Created by oksana on 07.06.16.
 */
public class Restaurant {
    private final List<Cuisine> cuisines;
    private final List<Product> drinks;
    private final List<Product> additives;

    private Restaurant(Builder builder) {
        cuisines = builder.cuisines;
        drinks = builder.drinks;
        additives = builder.additives;
    }

    public List<Cuisine> getCuisines() {
        return cuisines;
    }

    public List<Product> getDrinks() {
        return drinks;
    }

    public List<Product> getAdditives() {
        return additives;
    }


    public static final class Builder {
        private List<Cuisine> cuisines;
        private List<Product> drinks;
        private List<Product> additives;

        public Builder() {
        }

        public Builder withCuisines(List<Cuisine> cuisines) {
            this.cuisines = cuisines;
            return this;
        }

        public Builder withDrinks(List<Product> drinks) {
            this.drinks = drinks;
            return this;
        }

        public Builder withAdditives(List<Product> additives) {
            this.additives = additives;
            return this;
        }

        public Restaurant build() {
            return new Restaurant(this);
        }
    }
}
